var taskManager = {
    _deletedTasks: [],
    _input: null,
    _warning: null,
    _taskHolder: null,

    TITLES: {
        DONE: 'Task done',
        REVERT: 'Return task!',
        DELETE: 'Delete!'
    },

    init: function(){
        this._taskHolder = $("#tasks");
        this._input = $("#task-name");
        this._warning = $("#warning-message");
        this._warning.hide();
        this._bindEvents();
    },

    getName: function(){
        return this._input.val();
    },

    clearName: function () {
        this._input.val("");
    },

    _bindEvents: function(){
        var it = this;
        $("#add-task").bind("click", jQuery.proxy(taskManager, 'addTask'));
        $("#return-task").bind("click", function () {
            if (it._deletedTasks.length) {
                it._deletedTasks.pop().show();
            }
        });
        $("#task-name").bind("click", function () {
            it._warning.hide();
        });
    },

    addTask: function () {
        var it = this;
        this._showHideWarning();
        var name = this.getName();
        if (!name) return;

        var task = $('<div/>', {
            class: "task"
        });
        var title = $('<p/>', {
            text: name
        });
        var doneButton = $('<button/>', {
            text: this.TITLES.DONE,
            click: this.taskHandler
        });
        var deleteButton = $('<button/>', {
            text: this.TITLES.DELETE,
            click: function(){
                task.hide();
                it._deletedTasks.push(task);
            }
        });
        task.append(title).append(doneButton).append(deleteButton);
        this._taskHolder.append(task);
        this.clearName();
    },

    taskHandler: function () {
        var task = $(this).parent();
        var titles = taskManager.TITLES;
        this.innerText = task.hasClass('completed') ? titles.DONE : titles.REVERT;
        task.toggleClass('completed');
    },

    _showHideWarning: function () {
        this.getName() ? this._warning.hide() : this._warning.show();
    }
};

document.addEventListener("DOMContentLoaded", function () {
    taskManager.init()
});